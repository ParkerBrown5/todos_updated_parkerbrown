package edu.towson.cosc431.brown.todos_parkerbrown

import android.app.Activity
import android.app.AlertDialog
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import java.io.File


class activity_main : AppCompatActivity(), ITodosController {
    private val scope= CoroutineScope(Dispatchers.Main)
    private lateinit var addTodoBtn: Button
    private lateinit var recyclerView: RecyclerView

    private var editingIdx = -1
    private lateinit var todosRepo: ITodoRepository
    private lateinit var todosRepo2: ITodoRepository

    override suspend fun toggleCompleted(pos: Int) {
        val todo = todosRepo.GetTodo(pos)
        todo.completed = !todo.completed
        todosRepo.UpdateTodo(pos, todo)
        recyclerView.adapter?.notifyItemChanged(pos)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.itemId->{
                val intent=Intent(this, settingsActivity::class.java)
                startActivity(intent)
                return true
            }else->{return false}
        }

    }

    override suspend fun editTodo(pos: Int) {
        editingIdx = pos
        val intent = Intent(this, NewTodoActivity::class.java)
        val todo = todosRepo.GetTodo(pos)
        intent.putExtra(NewTodoActivity.TODO, Gson().toJson(todo))
        startActivityForResult(intent,
            REQUEST_CODE
        )
    }

    override suspend fun deleteTodo(pos: Int) {
        todosRepo.DeleteTodo(pos)
        recyclerView.adapter?.notifyItemRemoved(pos)


    }


    override fun getTodo(pos: Int): Todo {
        return todosRepo.GetTodo(pos)
    }

    override fun getTodoCount(): Int {
        return todosRepo.GetCount()
    }

    override fun runAsync(block: suspend () -> Unit) {
        scope.launch { block() }
    }

    private fun confirmDelete(callback: () -> Unit) {
        AlertDialog
            .Builder(this)
            .setMessage("Delete Todo?")
            .setPositiveButton("Delete") { _, _ ->
                callback()
            }
            .setNegativeButton("Cancel") { _, _ -> }
            .create()
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val prefs= getPreferences(Context.MODE_PRIVATE)
        val shared= PreferenceManager.getDefaultSharedPreferences(this)
        val isChecked= prefs.getBoolean("pref", true)

        val todoApi= api()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout)



        addTodoBtn = findViewById(R.id.add_todo_btn)
        recyclerView = findViewById(R.id.recyclerView)

        todosRepo = TodoDBRepository(application, isChecked)
        runAsync {
            val todos = todoApi.fetchList()
            todos.forEach{ todo ->
                val flag1=true
                var int=0
                todos.forEach{todo2->
                if(todo.id==todo2.id)
                    int+=1
                }
            if(int<2){todosRepo.AddTodo(todo)}}
        }






        runAsync {
        todosRepo.GetTodos()}
        addTodoBtn.setOnClickListener { launchNewTodoActivity() }
        recyclerView.adapter =
            TodoAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)

        recyclerView.scrollToPosition(0)

        recyclerView.adapter?.notifyDataSetChanged()




    }
    override fun storeCache(filename: String, icon: Bitmap){
        val file= File(cacheDir, filename)
        val output= file.outputStream()
        icon.compress(Bitmap.CompressFormat.JPEG, 100, output)

    }
    override fun checkCache(filename: String): Bitmap?{
        val file= File(cacheDir, filename)
        return if(file.exists()){
            val x= BitmapFactory.decodeStream(file.inputStream())
            return x
        }
        else{
            return null
        }
    }


    private fun launchNewTodoActivity() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent,
            REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            REQUEST_CODE -> {
                when(resultCode) {
                    Activity.RESULT_OK -> {

                            val todo = Gson().fromJson<Todo>(
                                data?.getStringExtra(NewTodoActivity.TODO),
                                Todo::class.java
                            )
                            if (editingIdx >= 0) {
                                runAsync{
                                todosRepo.UpdateTodo(editingIdx, todo)
                                    delay(10)
                                }

                                recyclerView.scrollToPosition(editingIdx)


                                recyclerView.adapter?.notifyDataSetChanged()

                            } else {


                                runAsync{

                                todosRepo.AddTodo(todo)}





                                recyclerView.scrollToPosition(0)
                                recyclerView.adapter?.notifyDataSetChanged()

                            }

                    }
                }
            }
        }
        editingIdx = -1
    }



    companion object {
        val REQUEST_CODE = 1
        val LIST = "todo_list"
        val TAG = activity_main::class.java.simpleName
    }

    override fun onStop(){
        super.onStop()

    }


}
