package edu.towson.cosc431.brown.todos_parkerbrown

import androidx.room.*
import java.util.*


@Dao
interface ITodoDao{

    @Query("select * from Todo")
    suspend fun getTodos(): List<Todo>

    @Query("select * from Todo where title = :title")
    suspend fun getTodo(title: String): Todo

    @Update
    suspend fun updateTodo(todo: Todo)

    @Delete
    suspend fun deleteTodo(todo: Todo)

    @Insert
    suspend fun addTodo(todo: Todo)

}

@Database(entities=[Todo::class], version = 1, exportSchema = false)
abstract class TodoDatabase: RoomDatabase(){
    abstract fun TodoDao(): ITodoDao
}