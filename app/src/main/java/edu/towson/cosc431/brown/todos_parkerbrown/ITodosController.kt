package edu.towson.cosc431.brown.todos_parkerbrown

import android.graphics.Bitmap

interface ITodosController {
    suspend fun toggleCompleted(pos: Int)
    suspend fun editTodo(pos: Int)
    suspend fun deleteTodo(pos: Int)
    fun getTodo(pos: Int): Todo
    fun getTodoCount(): Int
    fun runAsync(block: suspend()-> Unit )
    fun checkCache(filename: String): Bitmap?
    fun storeCache(filename: String, icon: Bitmap)
}