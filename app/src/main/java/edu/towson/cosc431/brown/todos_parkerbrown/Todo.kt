package edu.towson.cosc431.brown.todos_parkerbrown

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Todo(

    var contents: String,

    var completed: Boolean,
    var title: String,
    var created: String,
    @SerializedName("icon_url")
    var image_url: String?,
    @PrimaryKey
    var id: String
){}