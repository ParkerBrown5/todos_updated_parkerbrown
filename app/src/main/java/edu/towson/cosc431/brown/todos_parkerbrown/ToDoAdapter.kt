package edu.towson.cosc431.brown.todos_parkerbrown

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView


class TodoAdapter(private val controller: ITodosController) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_cardview_layout, parent, false)
        val vh = TodoViewHolder(view)

        vh.checkbox.setOnClickListener {
            val position = vh.adapterPosition
            controller.runAsync {
            controller.toggleCompleted(position)}
        }

        view.setOnClickListener {
            controller.runAsync{
            controller.editTodo(vh.adapterPosition)}

        }

        view.setOnLongClickListener {
             controller.runAsync{
            controller.deleteTodo(vh.adapterPosition)}
            true
        }

        return vh
    }

    override fun getItemCount(): Int {
        if (controller.getTodoCount()==0){
            Thread.sleep(100)
        }
        return controller.getTodoCount()
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {

        val todo = controller.getTodo(position)
        holder.bindTodo(todo, controller)
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val checkbox: CheckBox = view.findViewById(R.id.checkbox)
    private val todo_title: TextView = view.findViewById(R.id.todo_title)
    private val todo_text: TextView = view.findViewById(R.id.todo_text)
    private val todo_create_date: TextView = view.findViewById(R.id.todo_create_date)
    private val imageView: ImageView= view.findViewById(R.id.imageView)

    fun bindTodo(todo: Todo, controller: ITodosController) {


        todo_title.text = todo.title
        todo_text.text = todo.contents
        checkbox.isChecked = todo.completed
        todo_create_date.text = todo.created
        val todoApi= api()
        val dummy=todo.image_url
        if(dummy!=null){

            controller.runAsync {
                var bitmap:Bitmap?= null
                if(controller.checkCache(dummy)==null) {
                    bitmap = todoApi.fetchIcon(dummy)
                    if(bitmap!= null){
                    controller.storeCache(dummy,bitmap)}
                }
                else{
                    bitmap=controller.checkCache(dummy)
                }
                imageView.setImageBitmap(bitmap)
            }
        }
        else{
            imageView.setImageBitmap(null)
        }

    }


}