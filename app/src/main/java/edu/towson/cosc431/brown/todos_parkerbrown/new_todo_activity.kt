package edu.towson.cosc431.brown.todos_parkerbrown

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.google.gson.Gson

import java.util.*

class NewTodoActivity : AppCompatActivity() {

    companion object {
        val TAG = NewTodoActivity::class.java.simpleName
        val TODO = "todo"
    }

    private var editing: Todo? = null
    private lateinit var titleEditText: EditText
    private lateinit var textEditText: EditText
    private lateinit var completedCheckbox: CheckBox
    private lateinit var titleText: TextView
    private lateinit var saveBtn: Button

    private fun makeTodo() : Todo {
        return Todo(
            contents = textEditText.editableText.toString(),
            title = titleEditText.editableText.toString(),
            completed = completedCheckbox.isChecked,
            id = UUID.randomUUID().toString(),
            created = Date().toString(),
            image_url= "https://picsum.photos/400/400?image=201"
        )
    }

    private fun saveTodo() {
        val todo = makeTodo()
        val intent = Intent()
        intent.putExtra(TODO, Gson().toJson(todo))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_todo_activity)

        findViews()

        if(intent.hasExtra(TODO)) {
            val todo = Gson().fromJson<Todo>(intent.getStringExtra(TODO), Todo::class.java)
            populateForm(todo)
        }

        saveBtn.setOnClickListener { saveTodo() }
    }

    private fun findViews() {
        titleEditText = findViewById(R.id.title_edit_text)
        textEditText = findViewById(R.id.text_edit_text)
        completedCheckbox = findViewById(R.id.newtodo_completed_checkbox)
        saveBtn = findViewById(R.id.save_btn)
        titleText = findViewById(R.id.new_todo_activity_title)
    }

    private fun populateForm(todo: Todo) {
        titleEditText.editableText.append(todo.title)
        textEditText.editableText.append(todo.contents)
        completedCheckbox.isChecked = todo.completed
        titleText.text = resources.getString(R.string.edit_todo)
    }
}
