package edu.towson.cosc431.brown.todos_parkerbrown

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CheckBox
import android.widget.Toast
import androidx.preference.PreferenceManager

class settingsActivity : AppCompatActivity() {
    private lateinit var settingsCb: CheckBox
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings)

        val prefs= getPreferences(Context.MODE_PRIVATE)
        val shared= PreferenceManager.getDefaultSharedPreferences(this)
        val isChecked= prefs.getBoolean("pref", false)
        val editor= shared.edit()

        settingsCb= findViewById(R.id.completedCB)
        settingsCb.isChecked=isChecked

        settingsCb.setOnClickListener{

            prefs.edit().putBoolean("pref", settingsCb.isChecked).apply()
        }


    }
}