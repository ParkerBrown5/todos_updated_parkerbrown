package edu.towson.cosc431.brown.todos_parkerbrown

import java.util.*

interface ITodoRepository {
    suspend fun GetTodos(): List<Todo>
    fun GetTodo(pos: Int): Todo
    suspend fun AddTodo(todo: Todo)
    suspend fun UpdateTodo(pos: Int, todo: Todo)
    suspend fun DeleteTodo(pos: Int)
    fun GetCount(): Int
}

class TodoMemRepository : ITodoRepository {
    private val todos = mutableListOf<Todo>()

    init {
        (0..10).forEach { i ->
            todos.add(Todo(
                contents = "Content $i",
                completed = false,
                title = "Title $i",
                id = i.toString(),
                created = Date().toString(),
                image_url = "google.com"

            ))
        }
    }

    override suspend fun GetTodos(): List<Todo> {
        return todos
    }

    override fun GetTodo(pos: Int): Todo {
        return todos[pos]
    }

    override suspend fun AddTodo(todo: Todo) {
        todos.add(0, todo)
    }

    override suspend fun UpdateTodo(pos: Int, todo: Todo) {
        todos.set(pos, todo)
    }

    override suspend fun DeleteTodo(pos: Int) {
        todos.removeAt(pos)
    }

    override fun GetCount(): Int {
        return todos.size
    }
}