package edu.towson.cosc431.brown.todos_parkerbrown

import android.app.Application
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TodoDBRepository(app: Application, ischecked: Boolean):ITodoRepository{

    val list: MutableList<Todo> = mutableListOf()
    private val isChecked=ischecked
    private val db: TodoDatabase

    init {

        db= Room.databaseBuilder(
            app,
            TodoDatabase::class.java,
            "Todo.db"
        ).allowMainThreadQueries().fallbackToDestructiveMigration().build()





    }

    override suspend fun GetTodos(): List<Todo> {
        if(GetCount()==0){
            clearAndFill()
        }
        return list
    }

    override fun GetTodo(pos: Int): Todo {
        return list[pos]
    }

    override suspend fun AddTodo(todo: Todo) {
        withContext(Dispatchers.IO){

        db.TodoDao().addTodo(todo)
        clearAndFill()}

    }

    override suspend fun UpdateTodo(pos: Int, todo: Todo) {
        withContext(Dispatchers.IO){
        db.TodoDao().updateTodo(todo )
        clearAndFill()}


    }

    override suspend fun DeleteTodo(pos: Int) {
        withContext(Dispatchers.IO){
        val dummy= list[pos]
        db.TodoDao().deleteTodo(dummy)
        clearAndFill()}
    }

    override fun GetCount(): Int {
        return list.size
    }

    suspend fun clearAndFill(){
        list.clear()
        list.addAll(db.TodoDao().getTodos())
    }



}