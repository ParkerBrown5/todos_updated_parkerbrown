package edu.towson.cosc431.brown.todos_parkerbrown

import android.app.NotificationChannel
import android.app.Service
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.Build
import android.os.IBinder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TodoService : JobService() {

    private val scope= CoroutineScope(Dispatchers.IO)

    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onStartJob(params: JobParameters?): Boolean{


        val db= TodoDBRepository(application, false)
        scope.launch {

        }

        return true

    }
    private fun createNotificationChannel(){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            val name= "TodoServiceChannel"

        }

    }
    companion object {
        val TAG= TodoService::class.simpleName
    }
}