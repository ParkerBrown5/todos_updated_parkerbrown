package edu.towson.cosc431.brown.todos_parkerbrown

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request

class api{
    private val urlfinal="https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"
    suspend fun fetchList(): List<Todo> {
        return withContext(Dispatchers.IO){
            val client=OkHttpClient()
            val request= Request.Builder(
            ).url(urlfinal).get().build()
            val response= client.newCall(request).execute()

            val json= response.body?.string()
            val gson= Gson()
            val listType= object :TypeToken<List<Todo>>() {}.type
            val todos=gson.fromJson<List<Todo>>(json,listType)
            todos
        }
    }
    suspend fun fetchIcon(url: String):Bitmap?{
        return withContext(Dispatchers.IO){
            val client= OkHttpClient()
            val request= Request.Builder(
            ).url(url).get().build()
            val response= client.newCall(request).execute()
            val bytestream= response.body?.byteStream()
            val bitmap= BitmapFactory.decodeStream(bytestream)
            bitmap
        }
    }


}